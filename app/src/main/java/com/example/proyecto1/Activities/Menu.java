package com.example.proyecto1.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyecto1.Api.Api;
import com.example.proyecto1.R;
import com.example.proyecto1.Servicios.servicioPeticion;
import com.example.proyecto1.ViewModels.Detalle_Procesador;
import com.example.proyecto1.ViewModels.Peticion_Procesadores;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {

    private TextView cerrarsesion;
    private ListView lista;
    ArrayList<String> Nombres=new ArrayList<>();
    final ArrayList<Integer> Idsprocesadores=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().hide();
        cerrarsesion=(TextView) findViewById(R.id.txtv1menu);
        lista=(ListView) findViewById(R.id.ltvMenu);

        cerrarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor= preferences.edit();
                editor.putString("Token","");
                editor.commit();
                startActivity(new Intent(Menu.this,Login.class));
            }
        });

        servicioPeticion service = Api.getApi(Menu.this).create(servicioPeticion.class);
        Call<Peticion_Procesadores> peticion_procesadoresCall= service.getProcesadores();
        peticion_procesadoresCall.enqueue(new Callback<Peticion_Procesadores>() {
            @Override
            public void onResponse(Call<Peticion_Procesadores> call, Response<Peticion_Procesadores> response) {
                Peticion_Procesadores peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(Menu.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){

                    for (int i = 0; i<peticion.procesadores.size()-1; i++){
                        Nombres.add(peticion.procesadores.get(i).nombre);
                        Idsprocesadores.add(peticion.procesadores.get(i).id);
                    }
                    ArrayAdapter<String> adapter =new ArrayAdapter<>(Menu.this,android.R.layout.simple_list_item_1,Nombres);
                    lista.setAdapter(adapter);

                }else{
                    Toast.makeText(Menu.this,peticion.procesadores.get(0).detalle,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Peticion_Procesadores> call, Throwable t) {
                Toast.makeText(Menu.this,"No podemos conectarnos :c",Toast.LENGTH_SHORT).show();
            }
        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerdatos(position);
            }
        });
    }
    public void obtenerdatos(int position){
                servicioPeticion service = Api.getApi(Menu.this).create(servicioPeticion.class);
                Call<Detalle_Procesador> detalleCall= service .detallesprocesador(Idsprocesadores.get(position));
                detalleCall.enqueue(new Callback<Detalle_Procesador>() {
                    @Override
                    public void onResponse(Call<Detalle_Procesador> call, Response<Detalle_Procesador> response) {
                        Detalle_Procesador peticion=response.body();
                        if(response.body()==null){
                            Toast.makeText(Menu.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
                            builder.setTitle("Nombre de procesador: " +peticion.procesador.nombre);
                            builder.setMessage("Tiene : "+peticion.procesador.nucleos +" nucleos\n" +
                                    "Su frecuencia es: "+peticion.procesador.frecuencia+"\n");
                            builder.setPositiveButton("Aceptar", null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }else{
                            Toast.makeText(Menu.this,peticion.procesador.detalle,Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Detalle_Procesador> call, Throwable t) {
                        Toast.makeText(Menu.this,"No podemos conectarnos :c",Toast.LENGTH_SHORT).show();
                    }
                });
    }

}

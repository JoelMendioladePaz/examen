package com.example.proyecto1.ViewModels;

import com.google.gson.annotations.SerializedName;

public class Procesadores {
    public String nombre;
    public int id;
    public String nucleos;
    public String frecuencia;
    public String detalle;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getUsername() {
        return nombre;
    }

    public void setUsername(String username) {
        this.nombre = username;
    }

    public String getNucleos() {
        return nucleos;
    }

    public void setNucleos(String nucleos) {
        this.nucleos = nucleos;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

}

package com.example.proyecto1.ViewModels;

import java.util.List;

public class Peticion_Procesadores {
    public String estado;
    public List<Procesadores> procesadores;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Procesadores> getProcesadores() {
        return procesadores;
    }

    public void setProcesadores(List<Procesadores> procesadores) {
        this.procesadores = procesadores;
    }


}


package com.example.proyecto1.ViewModels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Detalle_Procesador {
    public int id;
    public String estado;
    public Procesadores procesador;

    public Detalle_Procesador(int id) {
        this.id = id;
    }

    public Procesadores getProcesador() {
        return procesador;
    }

    public void setProcesador(Procesadores procesador) {
        this.procesador = procesador;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
